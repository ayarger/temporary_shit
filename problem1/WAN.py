"""
A network topology representing a wide-area network.
Two hosts connected via a cloud of switches.
HW1 (1)
"""
import argparse

from mininet.net import Mininet
from mininet.node import Controller, OVSSwitch, NOX, Ryu, RemoteController
from mininet.util import dumpNodeConnections
from mininet.log import setLogLevel
from mininet.cli import CLI

def run_wan_topo(remote_controller=False):
    net = None
    if remote_controller:
	net = Mininet(switch=OVSSwitch, autoSetMacs='True', controller=RemoteController)
    else:
	net = Mininet(switch=OVSSwitch, autoSetMacs='True', controller=Controller)

    # Create a controller
    c0 = None
    if remote_controller:
        c0 = net.addController('c0',  port=6633, ip='0.0.0.0', dpid='77', controller=RemoteController)
    else:
        c0 = net.addController('c0', controller=Controller, dpid='77', protocols=["OpenFlow13"])
	

    # Create switches.
    SW1 = net.addSwitch('SW1', protocols=["OpenFlow13"], dpid='1')
    SW2 = net.addSwitch('SW2', protocols=["OpenFlow13"], dpid='2')
    SW3 = net.addSwitch('SW3', protocols=["OpenFlow13"], dpid='3')
    SW4 = net.addSwitch('SW4', protocols=["OpenFlow13"], dpid='4')
    SW5 = net.addSwitch('SW5', protocols=["OpenFlow13"], dpid='5')
	
    # Create hosts.
    H1 = net.addHost('H1', ip='192.168.1.1/8', dpid='101')
    H2 = net.addHost('H2', ip='192.168.1.2/8', dpid='102')

    # Create links.
    net.addLink(H1, SW5, port1=1, port2=1)
    net.addLink(H2, SW1, port1=1, port2=1)

    net.addLink(SW1, SW2, port1=2, port2=1)
    net.addLink(SW1, SW3, port1=3, port2=1)
    net.addLink(SW2, SW3, port1=2, port2=2)
    net.addLink(SW3, SW4, port1=3, port2=1)
    net.addLink(SW3, SW5, port1=4, port2=2)
    net.addLink(SW4, SW5, port1=2, port2=3)

    net.build()
    net.start()
    c0.start()
    print "WAN started..."
    CLI(net)
    c0.stop()
    net.stop()

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--controller-stat', dest='controller_type', default="default")
    args = parser.parse_args()	
    print(args.controller_type)
    setLogLevel('info')
    run_wan_topo(remote_controller=(args.controller_type == "remote"))
