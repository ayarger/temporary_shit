"""
A network topology representing a home network.
Three hosts connected to one switch.
HW1 (1)
"""
#TODO: If remote controller is specified, how do we know what IP to give it?
import argparse

from mininet.net import Mininet
from mininet.node import Controller, OVSSwitch, RemoteController
from mininet.util import dumpNodeConnections
from mininet.log import setLogLevel
from mininet.cli import CLI

def run_home_network_topo(remote_controller=False):
    net = None
    if remote_controller:
    	net = Mininet(switch=OVSSwitch, autoSetMacs='True', controller=RemoteController)
    else:
    	net = Mininet(switch=OVSSwitch, autoSetMacs='True', controller=Controller)

	# Create a controller
    c0 = None
    if remote_controller:
        c0 = net.addController('c0',  port=6633, ip='0.0.0.0', dpid='77', controller=RemoteController)
    else:
        c0 = net.addController('c0', controller=Controller, dpid='77', protocols=["OpenFlow13"])

    # Create a switch.
    SW0 = None
    if remote_controller:
	SW0 = net.addSwitch('SW0', protocols=["OpenFlow13"], dpid='7')
    # The default POX controller doesn't support OpenFlow13 (Per SDNhub.org), and fails when we try to use it in our switches. Thus, we simply use the default OpenFlow version when not connecting to RYU.
    else:
        SW0 = net.addSwitch('SW0', dpid='7')

    # Create hosts.
    H1 = net.addHost('H1', ip='192.168.1.1/8', dpid='11', mac="00:00:00:00:00:01")
    H2 = net.addHost('H2', ip='192.168.1.2/8', dpid='12', mac="00:00:00:00:00:02")
    H3 = net.addHost('H3', ip='192.168.1.3/8', dpid='13', mac="00:00:00:00:00:03")

    # Create links.
    net.addLink(H1, SW0, port1=1, port2=1)
    net.addLink(H2, SW0, port1=1, port2=2)
    net.addLink(H3, SW0, port1=1, port2=3)

    net.build()
    net.start()
    c0.start()
    print "HomeNet started..."
    CLI(net)
    c0.stop()
    net.stop()

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--controller-stat', dest='controller_type', default="default")
    args = parser.parse_args()	
    print(args.controller_type)
    setLogLevel('info')
    run_home_network_topo(remote_controller=(args.controller_type == "remote"))
