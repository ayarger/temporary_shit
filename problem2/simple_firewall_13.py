# Note that this RYU application is extended from the Mac learning app ryu/app/simple_switch_13.py as specified in HW1.
# Ryu-book was used in research for this problem. For example, discovering the structure of a FlowStatsPacket.

from ryu.base import app_manager
from ryu.controller import ofp_event
from ryu.controller.handler import CONFIG_DISPATCHER, MAIN_DISPATCHER
from ryu.controller.handler import set_ev_cls
from ryu.ofproto import ofproto_v1_3
from ryu.lib.packet import packet
from ryu.lib.packet import ethernet
from ryu.lib.packet import ether_types
from ryu.lib import hub
from ryu.topology.event import EventSwitchEnter

class SimpleFirewall13(app_manager.RyuApp):
    OFP_VERSIONS = [ofproto_v1_3.OFP_VERSION]

    def __init__(self, *args, **kwargs):
        super(SimpleFirewall13, self).__init__(*args, **kwargs)
        self.mac_to_port = {}
        self.datapaths = {}
        
        # separate-threads are used for monitoring / logging stats.
        self.monitor_thread = hub.spawn(self._monitor)
        self.monitor_sleep_interval = 0.01
        
        # stats file logging.
        self.monitor_filename = "simple_firewall_13_stats.txt" 
        self.previous_1_to_3_packet_count = 0

	# This loop will allow us to monitor statistics over time.
	# We send a stats request to the controller every interval, and handle the reply.
    def _monitor(self):
        while True:
            for dp in self.datapaths.values():
            	# Send a stats request to the switch
                parser = dp.ofproto_parser
        		req = parser.OFPFlowStatsRequest(dp)
        		dp.send_msg(req)
        		
            # Pause between requests.
            hub.sleep(self.monitor_sleep_interval)

    @set_ev_cls(ofp_event.EventOFPSwitchFeatures, CONFIG_DISPATCHER)
    def switch_features_handler(self, ev):
        datapath = ev.msg.datapath
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser
        
        # install table-miss flow entry
        #
        # We specify NO BUFFER to max_len of the output action due to
        # OVS bug. At this moment, if we specify a lesser number, e.g.,
        # 128, OVS will send Packet-In with invalid buffer_id and
        # truncated packet data. In that case, we cannot output packets
        # correctly.  The bug has been fixed in OVS v2.1.0.
        match = parser.OFPMatch()
        actions = [parser.OFPActionOutput(ofproto.OFPP_CONTROLLER,
                                          ofproto.OFPCML_NO_BUFFER)]
        self.add_flow(datapath, 0, match, actions)

    def add_flow(self, datapath, priority, match, actions, buffer_id=None):
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

        inst = [parser.OFPInstructionActions(ofproto.OFPIT_APPLY_ACTIONS,
                                             actions)]
        if buffer_id:
            mod = parser.OFPFlowMod(datapath=datapath, buffer_id=buffer_id,
                                    priority=priority, match=match,
                                    instructions=inst)
        else:
            mod = parser.OFPFlowMod(datapath=datapath, priority=priority,
                                    match=match, instructions=inst)
        datapath.send_msg(mod)

    @set_ev_cls(ofp_event.EventOFPFlowStatsReply, MAIN_DISPATCHER)
    def _stats_in_handler(self, ev):
        """
        Process stats retrieved from the switch.
        vSwitches keep their own stats. This is how we access them.
        """
        body = ev.msg.body
        packet_count = 0

        # Body contains a number of flow-stat objects.
        # Each object contains statistics on a flow.
        for flow_stat in body:
            # Non-priority1 stats seem to lack some of the properties we need for some reason. Quality documentation on this does not seem to exist...
            if flow_stat.priority != 1:
                continue

            if len(flow_stat.instructions) <= 0:
                continue
            in_port = flow_stat.match['in_port']
            if len(flow_stat.instructions) <= 0:
                continue
            out_port = flow_stat.instructions[0].actions[0].port
            if in_port == 1 and out_port == 3 or in_port == 3 and out_port == 1:
                packet_count += flow_stat.packet_count
 
        # Record information into file.
        if packet_count != self.previous_1_to_3_packet_count:
            print "H1-H3 traffic: " + str(packet_count)
            # The following line prevents us from writing to disk every 1/4 second.
            self.previous_1_to_3_packet_count = packet_count
            with open(self.monitor_filename, 'w') as f:
                f.write("Packets between H1 and H3:\n" + str(packet_count) + '\n')

    @set_ev_cls(ofp_event.EventOFPPacketIn, MAIN_DISPATCHER)
    def _packet_in_handler(self, ev):
        # If you hit this you might want to increase
        # the "miss_send_length" of your switch
        if ev.msg.msg_len < ev.msg.total_len:
            self.logger.debug("packet truncated: only %s of %s bytes",
                              ev.msg.msg_len, ev.msg.total_len)
        msg = ev.msg
        datapath = msg.datapath
        if not datapath.id in self.datapaths:
            self.datapaths[datapath.id] = datapath

        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser
        in_port = msg.match['in_port']

        pkt = packet.Packet(msg.data)
        eth = pkt.get_protocols(ethernet.ethernet)[0]

        if eth.ethertype == ether_types.ETH_TYPE_LLDP:
            # ignore lldp packet
            return
        dst = eth.dst
        src = eth.src

        dpid = datapath.id
        self.mac_to_port.setdefault(dpid, {})

        # learn a mac address to avoid FLOOD next time.
        self.mac_to_port[dpid][src] = in_port

        if dst in self.mac_to_port[dpid]:
            out_port = self.mac_to_port[dpid][dst]
        else:
            out_port = ofproto.OFPP_FLOOD

		# After obtaining the source and destination MAC addresses, we can drop packets if they traverse from H1 to H2 or vice-versa. 
		# Ideally, this kind of information would not be hard-coded, but fed in via the command-line to allow for flexible blocking of traffic. 
	
        # An empty action list tells a switch to DROP the packet.
        actions = None
        if src == "00:00:00:00:00:01" and dst == "00:00:00:00:00:02" or src=="00:00:00:00:00:02" and dst == "00:00:00:00:00:01":
	    print "DROPPING packet!" # Drop the packet by doing nothing.
	    actions = []
        else:
            actions = [parser.OFPActionOutput(out_port)]

        # install a flow to avoid packet_in next time
        if out_port != ofproto.OFPP_FLOOD:
            match = parser.OFPMatch(in_port=in_port, eth_dst=dst)
            # verify if we have a valid buffer_id, if yes avoid to send both
            # flow_mod & packet_out
            if msg.buffer_id != ofproto.OFP_NO_BUFFER:
                self.add_flow(datapath, 1, match, actions, msg.buffer_id)
                return
            else:
                self.add_flow(datapath, 1, match, actions)
        data = None
        if msg.buffer_id == ofproto.OFP_NO_BUFFER:
            data = msg.data

        out = parser.OFPPacketOut(datapath=datapath, buffer_id=msg.buffer_id,
                                  in_port=in_port, actions=actions, data=data)

	datapath.send_msg(out)
