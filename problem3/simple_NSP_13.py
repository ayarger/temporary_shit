# Note that this RYU application is extended from the Mac learning app ryu/app/simple_switch_13.py as specified in HW1.
# Ryu-book was used in research for this problem. For example, discovering the structure of a FlowStatsPacket.

from ryu.base import app_manager
from ryu.controller import ofp_event
from ryu.controller.handler import CONFIG_DISPATCHER, MAIN_DISPATCHER
from ryu.controller.handler import set_ev_cls
from ryu.ofproto import ofproto_v1_3
from ryu.lib.packet import packet
from ryu.lib.packet import ethernet
from ryu.lib.packet import ether_types
from ryu.lib.packet import arp
from collections import defaultdict
from ryu.topology import event, api
class SimpleSwitch13(app_manager.RyuApp):
    OFP_VERSIONS = [ofproto_v1_3.OFP_VERSION]

    def __init__(self, *args, **kwargs):
        super(SimpleSwitch13, self).__init__(*args, **kwargs)
        self.mac_to_port = {}
        self.dpid_to_ips = defaultdict(list) # dpid -> list of IPs connected (attachement points).
        self.ips_to_dpid = {} # IP -> dpid. For grabbing the attachement point given a host.
        self.attachment_points = []
        self.datapaths = {}
        self.links = defaultdict(list)

        self.source_ip_to_dpid = {} # Maps source IP to Attaching datapath.
        self.dpid_to_attachment_port = {} # dpid -> attachment port. This helps us remember how to forward packets to the host.
        self.ip_to_mac = {} # Keep track of Host MAC addresses.

    @set_ev_cls(ofp_event.EventOFPSwitchFeatures, CONFIG_DISPATCHER)
    def switch_features_handler(self, ev):
        datapath = ev.msg.datapath
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

        # install table-miss flow entry
        #
        # We specify NO BUFFER to max_len of the output action due to
        # OVS bug. At this moment, if we specify a lesser number, e.g.,
        # 128, OVS will send Packet-In with invalid buffer_id and
        # truncated packet data. In that case, we cannot output packets
        # correctly.  The bug has been fixed in OVS v2.1.0.
        match = parser.OFPMatch()
        actions = [parser.OFPActionOutput(ofproto.OFPP_CONTROLLER,
                                          ofproto.OFPCML_NO_BUFFER)]
        self.add_flow(datapath, 0, match, actions)

    def add_flow(self, datapath, priority, match, actions, buffer_id=None):
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

        inst = [parser.OFPInstructionActions(ofproto.OFPIT_APPLY_ACTIONS,
                                             actions)]
        if buffer_id:
            mod = parser.OFPFlowMod(datapath=datapath, buffer_id=buffer_id,
                                    priority=priority, match=match,
                                    instructions=inst)
        else:
            mod = parser.OFPFlowMod(datapath=datapath, priority=priority,
                                    match=match, instructions=inst)
        datapath.send_msg(mod)

    @set_ev_cls(event.EventLinkAdd)
    def event_link_add_handler(self, ev):
        srcid, dstid = ev.link.src.dpid, ev.link.dst.dpid
        srcpt, dstpt = ev.link.src.port_no, ev.link.dst.port_no

        # Acquire datapaths
        src_switch = api.get_switch(self, ev.link.src.dpid)[0]
        src_datapath = src_switch.dp

        # Record datapath
        self.datapaths[srcid] = src_datapath

        if dstid not in self.links[srcid]:
            self.links[srcid].append(dstid)

    @set_ev_cls(event.EventLinkDelete)
    def event_link_delete_handler(self, ev):
        srcid, dstid = ev.link.src.dpid, ev.link.dst.dpid
        srcpt, dstpt = ev.link.src.port_no, ev.link.dst.port_no

        if dstid in self.links[srcid]:
            self.links[srcid].remove(dstid)

    def add_flows_for_path_between_hosts(self, source_ip, dest_ip):
        start_dpid = self.ips_to_dpid[source_ip]
        finish_dpid = self.ips_to_dpid[dest_ip]
        path = discover_non_shortest_path(start_dpid, finish_dpid)
        reversed_path = path.reverse()

        print "path: " + str(path)
        print "reversed_path: " + str(reversed_path)

    def depth_first_search(self, current_stack, discovered_paths, finish_dpid):
        potential_next_nodes = self.links[current_stack[-1]]
        for next_node in potential_next_nodes:
            if next_node in current_stack:
                continue

            stack_copy = copy(current_stack)
            stack_copy.append(next_node)
        
            # Potential finish
            if stack_copy[-1] == finish_dpid:
                discovered_paths.append(stack_copy)
            else:
                self.depth_first_search(stack_copy, discovered_paths)

    def discover_non_shortest_path(self, start_dpid, finish_dpid):
        discovered_paths = []
        self.depth_first_search([start_dpid], discovered_paths, finish_dpid)
        
        # Remove shortest path
        smallest_length = 999999999
        for path in discovered_paths:
            if len(path) < smallest_length:
                smallest_length = len(path)

        shortest_paths = []
        other_paths = []
        for path in discovered_paths:
            if len(path) == smallest_length:
                shortest_paths.append(path)
            elif len(path) > smallest_length:
                other_paths.append(path)

        if len(other_paths) > 0:
            with open(self.log_filename, 'w') as f:
                f.write(str(other_paths))
            return choice(other_paths)
        else:
            with open(self.log_filename, 'w') as f:
                f.write(str(other_paths))
            return choice(shortest_paths)

    @set_ev_cls(ofp_event.EventOFPPacketIn, MAIN_DISPATCHER)
    def _packet_in_handler(self, ev):
        msg = ev.msg
        datapath = msg.datapath
        if not datapath.id in self.datapaths:
            self.datapaths[datapath.id] = datapath
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser
        in_port = msg.match['in_port']

        pkt = packet.Packet(msg.data)
        eth = pkt.get_protocols(ethernet.ethernet)[0]

        if eth.ethertype == ether_types.ETH_TYPE_LLDP:
            # ignore lldp packet
            return
        dst = eth.dst
        src = eth.src

        dpid = datapath.id
        self.mac_to_port.setdefault(dpid, {})

        # Grab ARP packets. We need to process these to prevent looping.
        arp_packet = pkt.get_protocol(arp.arp)
        if arp_packet is not None:
            source_ip_address = arp_packet
            destination_ip_address = arp_packet.dst_ip

            source_mac_address = arp_packet.src_mac
            destination_mac_address = arp_packet.dst_mac
 
            #print("dst: " + str(dst) + " d_mac: " + str(destination_mac_address))
            #print("src: " + str(src) + " s_mac: " + str(source_mac_address))

            adjacent_to_host = (source_mac_address == eth.src)

            # Keep track of attachment points.
            # Note that the first time we see an ARP with a particular source_ip,
            # we know that this datapath is adjacent to the source-ip host.
            if source_ip_address not in self.source_ip_to_dpid.keys():
                self.source_ip_to_dpid[source_ip_address] = dpid
                self.dpid_to_attachment_port[dpid] = in_port
            
            self.ip_to_mac[destination_ip_address] = destination_mac_address
            self.ip_to_mac[source_ip_address] = source_mac_address

            # Program topology with path.
            # If we have registered the dpid of both hosts, we can calculate a path.
            if source_ip_address in self.ips_to_dpid.keys() and destination_ip_address in self.ips_to_dpid.keys(): 
                self.add_flows_for_path_between_hosts(source_ip_address, destination_ip_address)

        # learn a mac address to avoid FLOOD next time.
        self.mac_to_port[dpid][src] = in_port

        if dst in self.mac_to_port[dpid]:
            out_port = self.mac_to_port[dpid][dst]
        else:
            out_port = ofproto.OFPP_FLOOD

        actions = [parser.OFPActionOutput(out_port)]

        # install a flow to avoid packet_in next time
        if out_port != ofproto.OFPP_FLOOD:
            match = parser.OFPMatch(in_port=in_port, eth_dst=dst)
            # verify if we have a valid buffer_id, if yes avoid to send both
            # flow_mod & packet_out
            if msg.buffer_id != ofproto.OFP_NO_BUFFER:
                self.add_flow(datapath, 1, match, actions, msg.buffer_id)
                return
            else:
                self.add_flow(datapath, 1, match, actions)
        data = None
        if msg.buffer_id == ofproto.OFP_NO_BUFFER:
            data = msg.data

        out = parser.OFPPacketOut(datapath=datapath, buffer_id=msg.buffer_id,
                                  in_port=in_port, actions=actions, data=data)
        datapath.send_msg(out)


