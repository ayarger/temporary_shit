# Note that this RYU application is extended from the Mac learning app ryu/app/simple_switch_13.py as specified in HW1.
# Ryu-book was used in research for this problem. For example, discovering the structure of a FlowStatsPacket.

from ryu.base import app_manager
from ryu.controller import ofp_event
from ryu.controller.handler import CONFIG_DISPATCHER, MAIN_DISPATCHER
from ryu.controller.handler import set_ev_cls
from ryu.ofproto import ofproto_v1_3
from ryu.lib.packet import packet
from ryu.lib.packet import ethernet
from ryu.lib.packet import ether_types
from ryu.lib import hub
from ryu.topology import event, api

from collections import defaultdict
from copy import copy
from random import choice

@set_ev_cls(ofp_event.EventOFPPacketIn, MAIN_DISPATCHER)
class SimpleNSP13(app_manager.RyuApp):
    OFP_VERSIONS = [ofproto_v1_3.OFP_VERSION]

    def __init__(self, *args, **kwargs):
        super(SimpleNSP13, self).__init__(*args, **kwargs)
        self.mac_to_port = {}
        # Record references to datapaths as we see them.
        self.datapaths = {}
        # We need links to traverse the topology.
        self._links = defaultdict(list)
        # Logging vars 
        self.log_filename = "simple_NSP_13_links.txt" 
        self._file_path_size = 0 
        
    @set_ev_cls(event.EventLinkAdd)
    def event_link_add_handler(self, ev):
        srcid, dstid = ev.link.src.dpid, ev.link.dst.dpid
        srcpt, dstpt = ev.link.src.port_no, ev.link.dst.port_no
        print "NEW LINK", srcid, srcpt, dstid, dstpt

        # Acquire datapaths
        src_switch = api.get_switch(self, ev.link.src.dpid)[0]
        src_datapath = src_switch.dp

        # Record datapath
        self.datapaths[srcid] = src_datapath

        if dstid not in self._links[srcid]:
            self._links[srcid].append(dstid)

    @set_ev_cls(event.EventLinkDelete)
    def event_link_delete_handler(self, ev):
        srcid, dstid = ev.link.src.dpid, ev.link.dst.dpid
        srcpt, dstpt = ev.link.src.port_no, ev.link.dst.port_no
        print "LOST LINK", srcid, srcpt, dstid, dstpt
        if dstid in self._links[srcid]:
            self._links[srcid].remove(dstid)

    @set_ev_cls(ofp_event.EventOFPSwitchFeatures, CONFIG_DISPATCHER)
    def switch_features_handler(self, ev):
        datapath = ev.msg.datapath
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

        # install table-miss flow entry
        #
        # We specify NO BUFFER to max_len of the output action due to
        # OVS bug. At this moment, if we specify a lesser number, e.g.,
        # 128, OVS will send Packet-In with invalid buffer_id and
        # truncated packet data. In that case, we cannot output packets
        # correctly.  The bug has been fixed in OVS v2.1.0.
        match = parser.OFPMatch()
        actions = [parser.OFPActionOutput(ofproto.OFPP_CONTROLLER,
                                          ofproto.OFPCML_NO_BUFFER)]
        self.add_flow(datapath, 0, match, actions)

    def add_flow(self, datapath, priority, match, actions, buffer_id=None):
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

        inst = [parser.OFPInstructionActions(ofproto.OFPIT_APPLY_ACTIONS,
                                             actions)]
        if buffer_id:
            mod = parser.OFPFlowMod(datapath=datapath, buffer_id=buffer_id,
                                    priority=priority, match=match,
                                    instructions=inst, idle_timeout=3.0,
                                    hard_timeout=3.0)
        else:
            mod = parser.OFPFlowMod(datapath=datapath, priority=priority,
                                    match=match, instructions=inst, 
                                    idle_timeout=3.0, hard_timeout=3.0)
        datapath.send_msg(mod) 

    def depth_first_search(self, current_stack, discovered_paths):
        potential_next_nodes = self._links[current_stack[-1]]
        for next_node in potential_next_nodes:
            if next_node in current_stack:
                continue

            stack_copy = copy(current_stack)
            stack_copy.append(next_node)
            
            # Potential finish
            if stack_copy[-1] == 5:
                discovered_paths.append(stack_copy)
            else:
                self.depth_first_search(stack_copy, discovered_paths)

    def discover_non_shortest_path(self):
        discovered_paths = []
        self.depth_first_search([1], discovered_paths)
        
        # Remove shortest path
        smallest_length = 999999999
        for path in discovered_paths:
            if len(path) < smallest_length:
                smallest_length = len(path)

        shortest_paths = []
        other_paths = []
        for path in discovered_paths:
            if len(path) == smallest_length:
                shortest_paths.append(path)
            elif len(path) > smallest_length:
                other_paths.append(path)

        # Write paths to file.
        if len(other_paths) > 0:
            if len(other_paths) > self._file_path_size:
                with open(self.log_filename, 'w') as f:
                    f.write(str(other_paths) + '\n')
                self._file_path_size = len(other_paths) 
            return choice(other_paths)
        else:
            if len(shortest_paths) > self._file_path_size:
                with open(self.log_filename, 'w') as f:
                    f.write(str(other_paths) + '\n')
                self._file_path_size = len(shortest_paths)
            return choice(shortest_paths)       

    @set_ev_cls(ofp_event.EventOFPPacketIn, MAIN_DISPATCHER)
    def _packet_in_handler(self, ev):
        # If you hit this you might want to increase
        # the "miss_send_length" of your switch
        if ev.msg.msg_len < ev.msg.total_len:
            self.logger.debug("packet truncated: only %s of %s bytes",
                              ev.msg.msg_len, ev.msg.total_len)
        msg = ev.msg
        datapath = msg.datapath
        if not datapath.id in self.datapaths:
            self.datapaths[datapath.id] = datapath

        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser
        in_port = msg.match['in_port']

        pkt = packet.Packet(msg.data)
        eth = pkt.get_protocols(ethernet.ethernet)[0]

        if eth.ethertype == ether_types.ETH_TYPE_LLDP:
            # ignore lldp packet
            return
        dst = eth.dst
        src = eth.src

        dpid = datapath.id
        self.mac_to_port.setdefault(dpid, {})
	
        print(self.discover_non_shortest_path())

        # learn a mac address to avoid FLOOD next time.
        self.mac_to_port[dpid][src] = in_port

        if dst in self.mac_to_port[dpid]:
            out_port = self.mac_to_port[dpid][dst]
        else:
            out_port = ofproto.OFPP_FLOOD

        actions = [parser.OFPActionOutput(out_port)]

        # install a flow to avoid packet_in next time
        if out_port != ofproto.OFPP_FLOOD:
            match = parser.OFPMatch(in_port=in_port, eth_dst=dst)
            # verify if we have a valid buffer_id, if yes avoid to send both
            # flow_mod & packet_out
            if msg.buffer_id != ofproto.OFP_NO_BUFFER:
                self.add_flow(datapath, 1, match, actions, msg.buffer_id)
                return
            else:
                self.add_flow(datapath, 1, match, actions)
        data = None
        if msg.buffer_id == ofproto.OFP_NO_BUFFER:
            data = msg.data

        out = parser.OFPPacketOut(datapath=datapath, buffer_id=msg.buffer_id,
                                  in_port=in_port, actions=actions, data=data)

	datapath.send_msg(out)
