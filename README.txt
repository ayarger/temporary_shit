HW1 has three parts. My submissions for each are detailed below...

===Problem 1===
Files: problem1/HomeNet.py, problem1/WAN.py

HomeNet.py
This file establishes a one-switch, three-host topology. Very simple. It has no issues establishing a ping with the default controller.

WAN.py
This file establishes a 5-switch, two-host, looping topology. This topology prevents the default and RYU simple_switch_13 controllers from establishing a ping.

** NOTE: Run the topologies with the command "sudo python HomeNet.py --controller-stat remote <or default>". Notice the lack of quotation marks around "remote" and "default" in the command. 

===Problem 2===
Files: problem2/simple_firewall_13.py

simple_firewall_13.py
This is the RYU firewall application. It listens on PacketIn events and installs a flow rule into any switch that sees a packet from H1 traversing to H2 (or vice versa). The flow rule commands the switch to drop these packets, while allowing traffic from H1 -> H3, H3 -> H2, etc.

This firewall application writes a file named "simple_firewall_13_stats.txt" to disk (in the same folder the program is in). The controller pings the switch every 1/100th of a second to request stats, and then writes the number of packets between Hosts H1 and H3 to the mentioned file (if the value needs updating). Ryu-Book documentation notified me of the existence of the FlowStatsRequest object and the FlowStatsReply event.

===Problem 3===
Files: problem3/simple_NSP_13.py, problem3/simple_NSP_shortest_paths.py

problem/simple_NSP_13.py
This file contains my best attempt at part (3). It contains logic for inspecting ARP packets, tracking attachment points, building a topology by listening to link-up and link-down events, and calculating and randomly selecting a non-shortest path (using Depth-First-Search). It is not functional, which is unfortunate given the effort I put in (can't win em all), but this did provide me an exceptional knowledge boost in the area of SDN.

problem3/simple_NSP_shortest_paths.py
One of the requirements of problem (3) is to write calculated shortest paths to a file. simple_NSP_shortest_paths.py provides this on a hard-coded copy of the WAN topology. It writes these paths to a file named "simple_NSP_13_paths.txt".

===Homework Feedback===
I believe this is the first time this homework has been given, and it's a good first shot-- If you'd like, consider the feedback for this homework below.

+ The Homework emphasized SDN, which thematically fits with the papers we've been reading.
+ Gained Mininet and Ryu experience seems like it will be useful for a number of student projects.
+ The amount of time given to the project this semester (several weeks) seems reasonable.

- The point distribution seems a little off, as part (3) receives as many points as the rest of the homework, but seems to be far, far more demanding in terms of time, effort, and work than the rest of the homework.
- RYU seems like a nice controller, but documentation / examples seem to be lacking. Lecture / discussion / Office Hours didn't provide enough of a foundation for constructing the functionality required in part (3). Unfortunately, there exist nearly-exact solutions online, and in the absence of good documentation this will draw students into an honesty gray area. What does one do if the only help you can find basically gives away the answers? (This is why my part (3) is incomplete-- I didn't want to use those sources).
- The Specification lacks some details. Several requirements mentioned in office hours don't seem to be mentioned in the Spec. For example, nothing is said as to the robustness / dynamicness required of the controller application. We know from office hours that the application should support topology changes and not be hard coded, but this should probably be in the spec. 

+----+      +----+
|     \ GO /     |
++  +  \  /  +  ++
 |  |\  \/  /|  | 
 |  | \    / |  | 
 |  |  \  /  |  | 
++  ++  \/  ++  ++ 
|    | BLUE |    |
+----+      +----+
-Diganta Saha